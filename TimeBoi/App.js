import React from 'react';
import {StyleSheet, Text, View, Button} from 'react-native';
import {createStackNavigator, createAppContainer } from 'react-navigation';
import ActivitiesScreen from './src/screens/ActivitiesScreen';

var TodaysDate = new Date();
var Month = TodaysDate.getUTCMonth() + 1;
var Day = TodaysDate.getUTCDate();
var Year = TodaysDate.getUTCFullYear();

var Weekdays = new Array(7);
Weekdays[0] =  "Sunday";
Weekdays[1] = "Monday";
Weekdays[2] = "Tuesday";
Weekdays[3] = "Wednesday";
Weekdays[4] = "Thursday";
Weekdays[5] = "Friday";
Weekdays[6] = "Saturday";
var Weekday = Weekdays[TodaysDate.getDay()];

var FullDate = ""+Day+"/"+Month+"/"+Year;

var WeekdaysMod = new Array(7);
WeekdaysMod[0] =  "Monday";
WeekdaysMod[1] = "Tuesday";
WeekdaysMod[2] = "Wednesday";
WeekdaysMod[3] = "Thursday";
WeekdaysMod[4] = "Friday";
WeekdaysMod[5] = "Saturday";
WeekdaysMod[6] = "Sunday";


AddWeekdayButtons = () =>
{
  var Buttons = [];

  var ReachedDay = false;
  var NextDayWarning = -1;
  for(var i = 0; i < 7; i++)
  {
    //set todays button as a different color
    var ColorBoi = "red";

    //past
    if(ReachedDay == false)
    {
        ColorBoi = "darkturquoise";
    }
    //future
    else
    {
      ColorBoi = "black";
    }

    //present day
    if(WeekdaysMod[i] === Weekday)
    {
        ColorBoi = "deepskyblue";
        ReachedDay = true;
        NextDayWarning = i+1;
    }

    //next day
    if(NextDayWarning == i)
    {
      ColorBoi = "black";
    }
    
    Buttons.push(<Button /*onPress={onPressLearnMore}*/ title={WeekdaysMod[i]} key={i} color={ColorBoi}/>);
  }
  return Buttons;
} 

class MainMenuScreen extends React.Component {
  render() {
    return (
      <View>
        <View style={styles.ButtonView}>
        <Text style={styles.welcome}>{FullDate}</Text>
        <Text style={styles.welcome}>Today is {Weekday}.</Text>
        {AddWeekdayButtons()}
        </View>
        <View style={styles.WeekTwoView}>
        <Button /*onPress={onPressLearnMore}*/ title="NEXT WEEK ->" color="purple"/>
        <Text style={styles.welcome}>-</Text>
        <Text style={styles.welcome}>Today you have a doctor's appointment at 9:30.</Text>
        <Text style={styles.welcome}>-</Text>
        <View style={styles.ViewActivitiesButton}>
        <Button onPress={() => NavigationEvents('Activity')} title="View Activities" color="blue"/>
        </View>
        </View>
      </View>
    );
  }
}

const AppNavigator = createStackNavigator({
  MainMenu: {screen: MainMenuScreen},
  Activity: {screen: ActivitiesScreen}
})

export default createAppContainer(AppNavigator);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  ButtonView: {
    textAlign: 'center',
    marginLeft: '10%',
    marginRight: '10%',
    width: '80%',
  },
  WeekTwoView: 
  {
    fontSize: 15,
    textAlign: 'center',
    marginLeft: '10%',
    marginRight: '10%',
    width: '80%'
  },
  ViewActivitiesButton: {
    textAlign: 'center',
    marginLeft: '20%',
    marginRight: '20%',
    width: '60%',
  },
});
