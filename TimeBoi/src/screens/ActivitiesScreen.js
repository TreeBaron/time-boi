import React from 'react';
import {StyleSheet, Text, View, Button} from 'react-native';
import { NavigationEvents} from 'react-navigation';


class ActivitiesScreen extends React.Component {
  render() {
    return (
      <View>
        <Text style={styles.welcome}>Today's Activities</Text>
        <Button /*onPress={onPressLearnMore}*/ title="Add Activity" color="blue"/>
        <Button onPress={() => NavigationEvents('MainMenu')} title="<- Back" color="blue"/>
      </View>
    );
  }
}

export default ActivitiesScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  ButtonView: {
    textAlign: 'center',
    marginLeft: '10%',
    marginRight: '10%',
    width: '80%',
  },
  WeekTwoView: 
  {
    fontSize: 15,
    textAlign: 'center',
    marginLeft: '10%',
    marginRight: '10%',
    width: '80%'
  },
  ViewActivitiesButton: {
    textAlign: 'center',
    marginLeft: '20%',
    marginRight: '20%',
    width: '60%',
  },
});
